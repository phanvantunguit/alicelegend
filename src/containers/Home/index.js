import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from './styled.module.scss';

const locales = {
  en: { title: 'English' },
  vn: { title: 'tiếng việt' },
};

const HomePage = () => {
  const { t, i18n } = useTranslation();
  return (
    <>
      <section className={styled.SectionOne}>
        <div className={styled.ConverBanner}>
          {/* <img
            src="/Photo/robert-collins-lP_FbBkMn1c-unsplash.jpg"
            height={'680px'}
          /> */}
        </div>
      </section>
      <section className={styled.SectionTwo}>
        <div className={styled.ConverBlock}>
          <div className={styled.BlockContentSectionTwoFirst}>
            <div className={styled.ConverImagesSectionTwo}>
              <img src="/Photo/school.png" width={'40px'} />
            </div>
            <h4>WELCOME</h4>
            <h4>TO ED SCHOOL</h4>
            <p>
              Ed's community is best described as a neighborhood where everyone
              looks after each other.
            </p>
          </div>
          <div className={styled.BlockContentSectionTwo}>
            <div className={styled.BlockChild}>
              <div
                className={styled.ConverImagesSectionTwo}
                style={{ backgroundColor: '#6c98e1' }}
              >
                <img src="/Photo/lifestyle.png" width={'40px'} />
              </div>
              <h4>PRACTICAL</h4>
              <h4>SCHOOL</h4>
              <p>
                The ED Community School District is made up of five elementary
                schools, one middle.
              </p>
            </div>
            <div className={styled.BlockChild}>
              <div
                className={styled.ConverImagesSectionTwo}
                style={{ backgroundColor: '#d93ad0 ' }}
              >
                <img src="/Photo/tent.png" width={'40px'} />
              </div>
              <h4>SENSORIAL</h4>
              <h4>ACTIVITIES</h4>
              <p>
                From our outstanding music program to our pre-engineering
                program and everything in between.
              </p>
            </div>
            <div className={styled.BlockChild}>
              <div
                className={styled.ConverImagesSectionTwo}
                style={{ backgroundColor: '#84c54e' }}
              >
                <img src="/Photo/artificial-intelligence.png" width={'40px'} />
              </div>
              <h4>LANGUAGE,</h4>
              <h4>SCIENCE...</h4>
              <p>
                Our schools offer excellent opportunities for students as they
                prepare for their future.
              </p>
            </div>
          </div>
        </div>
      </section>
      {/* SectionThree */}
      <section className={styled.SectionThree}>
        <div className={styled.ConverContentSectionThree}>
          <div className={styled.ConverBlockLeft}>
            <h5>LEARNING FACILITIES INCLUDE</h5>
            <ul>
              <li>After School</li>
              <li>After School Fine Arts</li>
              <li> Athletics</li>
              <li>Resource Room</li>
              <li>Dismissal Procedures</li>
              <li>Dining Hall</li>
              <li>College Planning Services</li>
              <li>Summer Camp Programs</li>
              <li>Two technology centers</li>
              <li>
                <a>Staff Directory</a>
              </li>
            </ul>
          </div>
          <div className={styled.ConverBlockRight}>
            <div className={styled.ConverBlockContentTop}>
              <div className={styled.ConverBlockContentSectionThree}>
                <div className={styled.BlockImageSectionthree}>
                  <img src="/Photo/01.jpeg" />
                </div>
                <div className={styled.ConverBlockTextSectionThree}>
                  <h5>
                    OUR DISTRICT &nbsp;<b>New</b>
                  </h5>
                  <p>
                    Our mission is to ensure success in all areas of
                    development.
                  </p>
                </div>
              </div>
              <div className={styled.ConverBlockContentSectionThree}>
                <div className={styled.BlockImageSectionthree}>
                  <img src="/Photo/02.jpeg" />
                </div>
                <div className={styled.ConverBlockTextSectionThree}>
                  <h5> LIBRARY</h5>
                  <p>
                    We are dedicated to providing a child-centered environment.
                  </p>
                </div>
              </div>
              <div className={styled.ConverBlockContentSectionThree}>
                <div className={styled.BlockImageSectionthree}>
                  <video controls autoPlay loop muted>
                    <source src="/Photo/video.mp4" type="video/mp4" />
                  </video>
                </div>
                <div className={styled.ConverBlockTextSectionThree}>
                  <h5>
                    OUR VIDEO &nbsp;<b>New</b>
                  </h5>
                  <p>
                    We are dedicated to provide a everyone looks after each
                    other.
                  </p>
                </div>
              </div>
            </div>
            <div className={styled.ConverBlockContentBottom}>
              <div className={styled.ConverBlockContentSectionThree}>
                {' '}
                <div className={styled.BlockImageSectionthree}>
                  <img src="/Photo/04.jpeg" />
                </div>
                <div className={styled.ConverBlockTextSectionThree}>
                  <h5>
                    SCHOOL FEES &nbsp;<b>New</b>
                  </h5>
                  <p>
                    Every day I see students throughout the district engaged in
                    stellar learning.
                  </p>
                </div>
              </div>
              <div className={styled.ConverBlockContentSectionThree}>
                {' '}
                <div className={styled.BlockImageSectionthree}>
                  <img src="/Photo/05.jpeg" />
                </div>
                <div className={styled.ConverBlockTextSectionThree}>
                  <h5> CALENDAR</h5>
                  <p>
                    As I walk through the school buildings, I am overwhelmed
                    with pride
                  </p>
                </div>
              </div>
              <div className={styled.ConverBlockContentSectionThreeLast}>
                <div
                  className={styled.ConverButtonColor}
                  style={{ background: '#d93ad0 ' }}
                >
                  <h5>GIVE ONLINE</h5>
                </div>
                <div
                  className={styled.ConverButtonColor}
                  style={{ background: '#6c98e1  ' }}
                >
                  <h5>APPLY NOW</h5>
                </div>
                <div
                  className={styled.ConverButtonColor}
                  style={{ background: '#84c54e  ' }}
                >
                  <h5>RANKING</h5>
                </div>
                <div
                  className={styled.ConverButtonColor}
                  style={{ background: '#e6be1e  ' }}
                >
                  <h5>ADMISSION</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default HomePage;
