import React, { useState, useEffect } from 'react';
import styled from './styled.module.scss';
import { useTranslation } from 'react-i18next';

const Header = () => {
  const { t, i18n } = useTranslation();
  const [visible, setVisible] = useState(false);
  const handleConverHeader = () => {
    setVisible(!visible);
  };

  const [offsetTop, setOffSetTop] = useState(false);
  const isTop = 85 || 0; // check position mouse
  const handleOnScroll = () => {
    if (window.pageYOffset > isTop) {
      setOffSetTop(true);
    } else {
      setOffSetTop(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleOnScroll);
    return () => {
      window.removeEventListener('scroll', handleOnScroll);
    };
  }, [isTop, handleOnScroll]);
  console.log(isTop);
  return (
    <>
      <div className={styled.WrapperHeader}>
        <div className={styled.WrapperContent}>
          <div className={styled.ConverLogoHeader}>
            <div className={styled.ConverFlowerMandala}>
              <img
                src="/Photo/Logo_flower_3d.png"
                height={'98px'}
                width={'98px'}
              />
            </div>
            <img
              src="/Photo/Text_Alice_cut.png"
              height={'75px'}
              width={'160px'}
            />
          </div>
          <div className={styled.ConverLogoHeaderMobile}>
            <img src="/Photo/Logo_flower_3d.png" height={'90px'} />
          </div>
          <div className={styled.ConverLogoHeaderMobileText}>
            <img src="/Photo/Text_Alice_cut.png" height={'50px'} />
          </div>
          <div className={styled.ConverEmail}>
            <div className={styled.ConverEmailIcon}>
              <img src="/Photo/Email.png" height={'25px'} width={'auto'} />
            </div>
            <p className={styled.ConverTextContact}>phanvantunguit@gmail.com</p>
          </div>
          <div className={styled.ConverPhoneNumber}>
            <div className={styled.ConverPIcon}>
              <img
                src="/Photo/clipart1101624.png"
                height={'15px'}
                width={'auto'}
              />
            </div>
            <p className={styled.ConverTextContact}>0866 978 880</p>
          </div>
          <div className={styled.ConverSignUp}>
            <button className={styled.ButtonSignIn}>
              {' '}
              {t('header.signin')}
            </button>
            <button className={styled.ButtonSignUp}>
              {' '}
              {t('header.signup')}
            </button>
          </div>
          <div className={styled.ConverMenuMobile}>
            <div
              className={styled.ConverMenuHover}
              onClick={() => handleConverHeader()}
            >
              <span className={styled.ConverIconBar}></span>
              <span
                className={
                  visible ? styled.ConverIconBar2 : styled.ConverIconBar
                }
              ></span>
              <span
                className={
                  visible ? styled.ConverIconBar3 : styled.ConverIconBar
                }
              ></span>
            </div>
          </div>
        </div>
      </div>
      <div className={styled.WrapperBottom}>
        <div className={styled.WrapperHeaderMenu}>
          <div className={styled.WrapperMenu}>
            <div
              className={
                offsetTop
                  ? styled.ConverLogoHeaderStickyScroll
                  : styled.ConverLogoHeaderSticky
              }
            >
              <div className={styled.ConverFlowerMandalaSticky}>
                <img
                  src="/Photo/Logo_flower_3d.png"
                  height={'70px'}
                  width={'70px'}
                />
              </div>
              <img
                src="/Photo/Text_Alice_cut.png"
                height={'60px'}
                width={'130px'}
              />
            </div>
            <div className={styled.blockItemHeader}>
              {/* Block child */}
              <div className={styled.dropdown}>
                <div className={styled.blockHeader}>
                  {t('header.about')}
                  <img src="/Photo/down.png" height={'12px'} width={'12px'} />
                </div>
                <div className={styled.dropdownContent}>
                  <a href="/about-company">Company</a>
                  <a href="/about-career">Career</a>
                  <a href="/about-contact">Contact</a>
                </div>
              </div>
            </div>
            {/* Teachers */}
            <div className={styled.blockItemHeader}>
              {/* Block child */}
              <div className={styled.dropdown}>
                <div className={styled.blockHeader}>
                  {t('header.teacher')}
                  <img src="/Photo/down.png" height={'12px'} width={'12px'} />
                </div>
                <div className={styled.dropdownContent}>
                  <a href="/about-company">Company</a>
                  <a href="/about-career">Career</a>
                  <a href="/about-contact">Contact</a>
                </div>
              </div>
            </div>
            {/* Course */}
            <div className={styled.blockItemHeader}>
              {/* Block child */}
              <div className={styled.dropdown}>
                <div className={styled.blockHeader}>
                  {t('header.course')}{' '}
                  <img src="/Photo/down.png" height={'12px'} width={'12px'} />
                </div>
                <div className={styled.dropdownContent}>
                  <a href="/about-company">Company</a>
                  <a href="/about-career">Career</a>
                  <a href="/about-contact">Contact</a>
                </div>
              </div>
            </div>
            {/* News */}
            <div className={styled.blockItemHeader}>
              {/* Block child */}
              <div className={styled.dropdown}>
                <div className={styled.blockHeader}>
                  {t('header.new')}
                  <img src="/Photo/down.png" height={'12px'} width={'12px'} />
                </div>
                <div className={styled.dropdownContent}>
                  <a href="/about-company">Company</a>
                  <a href="/about-career">Career</a>
                  <a href="/about-contact">Contact</a>
                </div>
              </div>
            </div>
            {/* Contact */}
            <div className={styled.blockItemHeader}>
              {/* Block child */}
              <div className={styled.dropdown}>
                <div className={styled.blockHeader}>{t('header.contact')}</div>
              </div>
            </div>
          </div>
          <div className={styled.WrapperLanguage}>
            <a className={styled.ConverMedia}>
              <img src="/Photo/facebook.png" height={'35px'} width={'35px'} />
            </a>
            <a className={styled.ConverMedia}>
              <img src="/Photo/youtube.png" height={'35px'} width={'35px'} />
            </a>
            <div className={styled.ConverMedia}>
              <select
                className={styled.ConverSelectLanguage}
                onChange={(e) => {
                  i18n.changeLanguage(e.target.value);
                }}
              >
                <option value="vn" className={styled.ConverVietnam}>
                  🇻🇳
                </option>
                <option value="en" className={styled.ConverEnglish}>
                  🏴󠁧󠁢󠁥󠁮󠁧󠁿
                </option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Header;
