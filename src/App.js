import './App.css';
import { BrowserRouter as Router, Routes, Link, Route } from 'react-router-dom';
import Home from './containers/Home';

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </>
  );
}

export default App;
